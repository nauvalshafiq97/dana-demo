import demo.EmployeeRequest;
import demo.EmployeeResponse;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

public class Employee {

    @Test
    public void getEmployee () {
        Response response = RestAssured
                .given()
                .baseUri("http://dummy.restapiexample.com") //base URL
                .basePath("/api") //base path
                .log()
                .all()
                .header("Content-type", "application/json") //create header
                .header("Accept", "application/json")
                .get("/v1/employees"); //get content

/*      response.getBody().prettyPrint(); //mengambil isi body request
        System.out.println(response.getStatusCode());
        Assert.assertEquals(200, response.getStatusCode());
        Assert.assertThat("This test takes too long", response.getTime(), Matchers.lessThan(3000L));
        Assert.assertEquals("success", response.path("status"));
        Assert.assertEquals("Tiger Nixon", response.path("data[0].employee_name"));*/

        EmployeeResponse employeeResponse = response.as(EmployeeResponse.class);
        System.out.println(employeeResponse.getData().get(1).getEmployeeName());
    }

    @Test
    public void createEmployee () {
        /*String requestBody = "{\n" +
                "  \"name\": \"Dana\",\n" +
                "  \"salary\": \"123\",\n" +
                "  \"age\": \"23\"\n" +
                "}";*/
        EmployeeRequest employeeRequest = new EmployeeRequest();
        employeeRequest.setName("Nauval Shafiq");
        employeeRequest.setAge("23");
        employeeRequest.setSalary("10000000");

        Response response = RestAssured
                .given()
                .baseUri("http://dummy.restapiexample.com")
                .basePath("/api")
                .log()
                .all()
                .header("Content-type", "application/json") //create header
                .header("Accept", "*/*")
                .body(employeeRequest)
                .post("/v1/create");

        //response.getBody().prettyPrint();
    }
}
